package com.oF2pks.applicationsinfo;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;

public class DetailActivity extends AppCompatActivity {

    private static final int LAYOUT_ID = 0x8898;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        FrameLayout frameLayout = new FrameLayout(this);
        frameLayout.setId(LAYOUT_ID);
        frameLayout.setBackgroundColor(getResources().getColor(R.color.grey_0));
        setContentView(frameLayout);
        String packageName = getIntent().getStringExtra(DetailFragment.EXTRA_PACKAGE_NAME);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(LAYOUT_ID, DetailFragment.getInstance(packageName))
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

ExodusPrivacy NEW database : 289 exoTrackers(250) = 221+68 signatures for 192+58 recensed trackers
More info : 
- https://reports.exodus-privacy.eu.org/api/trackers (221 signatures for 192 effective entities)
- https://gitlab.com/oF2pks/3xodusprivacy-toolbox (68 known additions °/in-standby ²/missing µ/tools)
- https://etip.exodus-privacy.eu.org/?page=1 
- https://etip.exodus-privacy.eu.org/trackers/export

# Exodus Escan(manifest) for ALL systemApps via ToggleList (including system ODEX packages...)

# ...ExodusPrivacy NEW database (eof448),

603 exoTrackers(455) = 574+29 signatures for 428+27 identified trackers

More info : 
- https://reports.exodus-privacy.eu.org/api/trackers
- https://gitlab.com/oF2pks/3xodusprivacy-toolbox (31 known additions ²/ETIP-standby °/missing µ/tools)
- https://etip.exodus-privacy.eu.org

